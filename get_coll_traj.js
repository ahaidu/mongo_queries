db.getCollection('oldpf_T_11_tf').aggregate([
                { "$match" : {"timestamp" : { "$gt" : 1.36, "$lte" : 40.1}}},
                
                { "$unwind" : "$models"},   
                { "$match" : {"models.name" : "Mondamin"}},
                { "$project" : {"models" : { "links" : 1 } , "timestamp" : 1, "_id" : 0}},
                
                { "$unwind" : "$models.links"},
                { "$match" : {"models.links.name" : "mondamin_link"}},
                { "$project" : {"models.links.collisions" : 1  , "timestamp" : 1}},
                
                { "$unwind" : "$models.links.collisions"},
                { "$match" : {"models.links.collisions.name" : "mondamin_event_collision"}},
                { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.links.collisions.pos",
                    "rot" : "$models.links.collisions.rot"
                    }}   
            ])   