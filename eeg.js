db.getCollection('RawData_an36').aggregate([
        { "$match" : { "$and" : 
            [
                {"eeg.c1" : { "$exists" : true}},
                {"timestamp" : { "$gt" : 0.0, "$lte" : 25.1}},
            ]}},            
        { "$sort" : { "timestamp" : 1 }},
        { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                     "eeg" : "$eeg.c1",
                    }},  
    ])