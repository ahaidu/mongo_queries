db.getCollection('oldpf_T_14_raw').aggregate([
        { "$match" : { "$and" : [
                {"timestamp" : { "$gt" : 1.36, "$lte" : 10.1}},
                {"models.name" : "LiquidTangibleThing"}
            ]}},            
        { "$sort" : { "timestamp" : 1 }},
        { "$unwind" : "$models"},
        { "$match" : {"models.name" : "LiquidTangibleThing"}},
        
        { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                    "pos" : "$models.pos",
                    "rot" : "$models.rot"
                    }},  
    ])