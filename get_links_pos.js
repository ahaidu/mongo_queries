db.getCollection('oldpf_T_11_tf').aggregate([
        { "$match" : { "$and" : [
                {"timestamp" : { "$lte" : 144.94}},
                {"models.name" : "LiquidTangibleThing"}
            ]}},            
        { "$sort" : { "timestamp" : -1 }},
        { "$limit" : 1 },
        { "$unwind" : "$models"},
        { "$match" : {"models.name" : "LiquidTangibleThing"}},
        
        { "$project" : {
                    "_id" : 0,
                    "timestamp" : 1 ,
                     "links_pos" : "$models.links.pos"
                    }},  
    ])